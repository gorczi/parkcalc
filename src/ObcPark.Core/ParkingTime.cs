using System;

namespace ObcPark.Core
{
    public class ParkingTime
    {
        public DateTime Start;
        public DateTime Stop;
        public bool Weekend;
        public TimeSpan Duration => Stop - Start;

        public ParkingTime(DateTime start, DateTime stop, bool weekend)
        {
            Start = start;
            Stop = stop;
            Weekend = weekend;
        }
    }
}