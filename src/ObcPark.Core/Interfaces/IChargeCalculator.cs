namespace ObcPark.Core.Interfaces

{
    public interface IChargeCalculator
    {
        double CalculateCharge(ParkingTime parkingTime);
    }
}