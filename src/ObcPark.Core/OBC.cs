﻿using ObcPark.Core.Interfaces;

namespace ObcPark.Core
{
    public class ObcParkA : ObcParkBase
    {
        public ObcParkA()
        {
            WeekendIsFree = false;
            FreeTime = 30;
            Period = 30;
            Time30_120 = 3;
            Time120_ = 5;
        }
    }

    public class ObcParkB : ObcParkBase
    {
        public ObcParkB()
        {
            WeekendIsFree = true;
            FreeTime = 30;
            Period = 30;
            Time30_120 = 2;
            Time120_ = 3;
        }
    }

    public class ObcParkC : ObcParkBase
    {
        public ObcParkC()
        {
            WeekendIsFree = true;
            FreeTime = 30;
            Period = 30;
            Time30_120 = 3;
            Time120_ = 5;
        }
    }

    public class ObcParkE : ObcParkBase
    {
        public ObcParkE()
        {
            WeekendIsFree = true;
            FreeTime = 30;
            Period = 30;
            Time30_120 = 3;
            Time120_ = 4;
        }
    }

    public abstract class ObcParkBase : IChargeCalculator
    {
        public bool WeekendIsFree { get; protected set; }
        public int FreeTime { get; protected set; }
        public int Period { get; protected set; }
        public int Time30_120 { get; protected set; }
        public int Time120_ { get; protected set; }

        public double CalculateCharge(ParkingTime parkingTime)
        {
            var parkedMinutes = parkingTime.Duration.Hours * 60 + parkingTime.Duration.Minutes;
            if (parkedMinutes <= 30)
                return 0;
            if (parkedMinutes <= 120)
            {
                return Calculate30_120(parkedMinutes);
            }
            else if (parkedMinutes > 120)
            {
                var costFirst120 = 3 * Time30_120;
                var minutesInZone = parkedMinutes - 120;

                var moduloTicks = minutesInZone % 30;
                var ticks = (minutesInZone - moduloTicks) / 30;
                var costAbove120 = ++ticks * Time120_;
                return costFirst120 + costAbove120;
            }
            return -1;
        }

        private double Calculate30_120(int parkedMinutes)
        {
            var minutesInZone = parkedMinutes - 30;
            var moduloTicks = minutesInZone % 30;
            var ticks = (minutesInZone - moduloTicks) / 30;
            return ++ticks * Time30_120;
        }
    }
}
