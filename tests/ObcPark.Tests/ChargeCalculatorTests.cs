using System;
using ObcPark.Core;
using Xunit;

namespace tests
{
    public class ParkingTest
    {
        ObcParkA obj = new ObcParkA();
        [Fact]
        public void ParkCalculatorObcParkFreeTime()
        {
            var obj = new ObcParkA();

            var parkingTime = new ParkingTime(
                new DateTime(2021, 1, 10, 10, 00, 00),
                new DateTime(2021, 1, 10, 10, 20, 00),
                false);

            var result = obj.CalculateCharge(parkingTime);
            Assert.Equal(0, result);
        }

        [Fact]
        public void ParkCalculatorObcParkOneTick()
        {
            var obj = new ObcParkA();

            var parkingTime = new ParkingTime(
                new DateTime(2021, 1, 10, 10, 00, 00),
                new DateTime(2021, 1, 10, 10, 40, 00),
                false);

            var result = obj.CalculateCharge(parkingTime);
            Assert.Equal(obj.Time30_120, result);
        }

        [Fact]
        public void ParkCalculatorObcParkTwoTicks()
        {
            var obj = new ObcParkA();

            var parkingTime = new ParkingTime(
                new DateTime(2021, 1, 10, 10, 00, 00),
                new DateTime(2021, 1, 10, 11, 10, 00),
                false);

            var result = obj.CalculateCharge(parkingTime);
            Assert.Equal(2 * obj.Time30_120, result);
        }

        [Fact]
        public void ParkCalculatorObcParkThreeTicks()
        {
            var parkingTime = new ParkingTime(
                new DateTime(2021, 1, 10, 10, 00, 00),
                new DateTime(2021, 1, 10, 11, 40, 00),
                false);

            var result = obj.CalculateCharge(parkingTime);
            Assert.Equal(3 * obj.Time30_120, result);
        }

        [Fact]
        public void ParkCalculatorObcParkFourTicks()
        {
            var parkingTime = new ParkingTime(
                new DateTime(2021, 1, 10, 10, 00, 00),
                new DateTime(2021, 1, 10, 12, 10, 00),
                false);

            var result = obj.CalculateCharge(parkingTime);
            Assert.Equal(3 * obj.Time30_120 + 1 * obj.Time120_, result);
        }

        [Fact]
        public void ParkCalculatorObcParkFiveTicks()
        {
            var parkingTime = new ParkingTime(
                new DateTime(2021, 1, 10, 10, 00, 00),
                new DateTime(2021, 1, 10, 12, 40, 00),
                false);

            var result = obj.CalculateCharge(parkingTime);
            Assert.Equal(3 * obj.Time30_120 + 2 * obj.Time120_, result);
        }
    }
}
